<?php
use App\Permission;
use App\Role;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/laratrust', function () {

    $owner               = new Role();
    $owner->name         = 'owner';
    $owner->display_name = 'Project Owner'; // optional
    $owner->description  = 'User is the owner of a given project'; // optional
    $owner->save();

    $admin               = new Role();
    $admin->name         = 'admin';
    $admin->display_name = 'User Administrator'; // optional
    $admin->description  = 'User is allowed to manage and edit other users'; // optional
    $admin->save();

    $createPost               = new Permission();
    $createPost->name         = 'create-post';
    $createPost->display_name = 'Create Posts'; // optional
    // Allow a user to...
    $createPost->description = 'create new blog posts'; // optional
    $createPost->save();

    $editUser               = new Permission();
    $editUser->name         = 'edit-user';
    $editUser->display_name = 'Edit Users'; // optional
    // Allow a user to...
    $editUser->description = 'edit existing users'; // optional
    $editUser->save();

    $admin->attachPermission($createPost); // parameter can be a Permission object, array or id
    // equivalent to $admin->permissions()->attach([$createPost->id]);

    $owner->attachPermissions([$createPost, $editUser]); // parameter can be a Permission object, array or id
    // equivalent to $owner->permissions()->attach([$createPost->id, $editUser->id]);

});

// Route::get('assign', function () {
//     $role = Role::find(1); //admin

//     $user = auth()->loginUsingId(1);

//     $user->attachRole($role);
// });

Route::get('test', function () {

    // $user = auth()->loginUsingId(1);

    // return $user->allPermissions();
    //
    $foods = ['Nasik Goreng', 'Goreng Pisang', 'Mee Goreng'];

    return array_first($foods);

});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
